package Models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.NaturalIdCache;


@Entity
@Table(name = "participants")

public class Participant {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idParticipant;
	private String Nom;

    public Participant() {
    	
    }
	public Participant(String nom) {
		Nom = nom;
	}
	
	
}
